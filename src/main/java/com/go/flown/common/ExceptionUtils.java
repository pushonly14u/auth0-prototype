package com.go.flown.common;

public class ExceptionUtils {

    public static class GlobalException extends RuntimeException{
        public GlobalException(String message) {
            super(message);
        }
    }
    public static class InvalidIdAndPasswordException extends RuntimeException{
        public InvalidIdAndPasswordException(String message) {
            super(message);
        }
    }
}
