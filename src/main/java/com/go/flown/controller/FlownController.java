package com.go.flown.controller;

import com.go.flown.common.LoginRequest;
import com.go.flown.service.FlownService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/flown")
public class FlownController {

    @Autowired
    private FlownService flownService;

    @GetMapping("/pub")
    public String pubEnd(){
        return flownService.getPublic();
    }

    @GetMapping("/pri")
    public String priEnd(){
        return flownService.getPrivate();
    }

    @GetMapping("/admin_pri")
    public String adminPri(){
        return flownService.getAdminPrivate();
    }

    @GetMapping("/user_pri")
    public String userPri(){
        return flownService.getUserPrivate();
    }

    @PostMapping("/token")
    public ResponseEntity<Object> getToken(@Validated @RequestBody LoginRequest request) throws Exception {
        return flownService.getToken(request);
    }

}
