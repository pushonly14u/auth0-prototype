package com.go.flown.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.go.flown.common.ExceptionUtils.GlobalException;
import com.go.flown.common.LoginRequest;
import com.go.flown.service.FlownService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.security.RolesAllowed;
import java.util.HashMap;
import java.util.Map;

import static com.go.flown.common.ExceptionUtils.*;
import static com.go.flown.common.Variables.ROLE_ADMIN;
import static com.go.flown.common.Variables.ROLE_USER;

@Service
public class FlownServiceImpl implements FlownService {
    @Autowired
    RestTemplate template;

    @Value(value = "${auth0.clientId}")
    private String clientId;
    @Value(value = "${auth0.clientSecret}")
    private String clientSecret;
    @Value(value = "${auth0.apiAudience}")
    private String audience;
    @Value(value = "${auth0.grantType}")
    private String grantType;
    @Value(value = "${auth0.scope}")
    private String scope;
    @Value(value = "${auth0.token_url}")
    private String tokenUrl;


    @Override
    public String getPublic() {
        return "Hello Public End-point";
    }

    @Override
    public String getPrivate() {
        return "You are VIP, Private Resource";
    }

    @Override
    public ResponseEntity<Object> getToken(LoginRequest request) throws Exception {

        HttpHeaders headers = new HttpHeaders();
        headers.set("content-type", "application/json");

        Map<String, String> body = new HashMap();
        body.put("client_id", clientId);
        body.put("client_secret", clientSecret);
        body.put("audience", audience);
        body.put("grant_type", grantType);
        body.put("username", request.getUsername());
        body.put("password", request.getPassword());

        ResponseEntity<Object> response = null;
        try {
            String reqBodyData = new ObjectMapper().writeValueAsString(body);
            HttpEntity<String> entity = new HttpEntity<>(reqBodyData, headers);
            response = template.postForEntity(tokenUrl, entity, Object.class);

        } catch (HttpClientErrorException.Forbidden e) {
            if (e.getMessage().contains("Wrong email or password")) {
                throw new InvalidIdAndPasswordException("You Entered Wrong email or password!!");
            } else {
                throw new GlobalException("Forbidden");
            }
        } catch (Exception e) {
            throw new GlobalException(e.getMessage());
        }
        return response;
    }

    @Override
    @RolesAllowed(ROLE_USER)
    public String getUserPrivate() {
        return "USER, You Welcome Man";
    }

    @Override
    @RolesAllowed(ROLE_ADMIN)
    public String getAdminPrivate() {
        return "ADMIN, Your Welcome Sir";
    }
}
