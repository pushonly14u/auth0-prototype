package com.go.flown.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.go.flown.common.LoginRequest;
import org.springframework.http.ResponseEntity;

public interface FlownService {
    String getPublic();
    String getPrivate();
    ResponseEntity<Object> getToken(LoginRequest request) throws JsonProcessingException, Exception;
    String getUserPrivate();
    String getAdminPrivate();
}
